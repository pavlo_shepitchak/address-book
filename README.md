
The app consists of the next modules:
1. usersDuck - for loading data information from api
2. settingsDuck - for storing settings (nationality field)
3. Home screen - for displaying list of users filtered by search query and nationality for settings 
4. Details screen - for displaying extended information about user in modal window
5. Settings screen - for setting nationality

Pay attention: 
- the app load pages with user in advance, but you can disable it by changing ENABLE_PRELOADING constant in config.js to evaluate an effort of this feature
- the app use lazy loading of user information data. Users list loads limited number of fields and then after clicking on user item more information about this user will be loaded.
But there is an issue: because an api doesn't support loading by id, every time you click on user item you'll see another information 


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

