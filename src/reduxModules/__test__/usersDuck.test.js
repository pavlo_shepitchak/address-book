import * as R from 'ramda';
import { TestScheduler } from 'rxjs/testing';
import usersDuck from '../usersDuck';

jest.mock('rxjs/ajax', () => {
  const Rx = require('rxjs');
  return { ajax: { getJSON: () => Rx.of({ result: [] }) } };
});

describe('usersDuck', () => {
  test('success', done => {
    const testScheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
      done();
    });
    testScheduler.run(({ hot, expectObservable }) => {
      const action$ = hot('-a', {
        a: usersDuck.actions.loadPage(0),
      });
      const store$ = R.objOf('value', null);
      const output$ = usersDuck.epic(action$, store$);
      expectObservable(output$).toBe('-a', {
        a: usersDuck.actions.loadPageSuccess({ result: [] }),
      });
    });
  });
});
