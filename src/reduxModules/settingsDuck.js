// @flow
import { handleActions } from 'redux-actions';
import * as R from 'ramda';
import { ReducerRegistry } from '../core';
import { createActionFactory, createGlobalizer } from '../utils';

const moduleName = 'SETTINGS';
const createAction = createActionFactory(moduleName);

const actions = {
  set: createAction('SET'),
};

const defaultState = {
  values: {},
};
const reducer = handleActions(
  {
    [actions.set.toString()]: (state, { payload }) => ({
      values: R.mergeRight(state.values, payload),
    }),
  },
  defaultState
);
ReducerRegistry.register(moduleName, reducer);

const selectors = createGlobalizer(moduleName)({
  getAll: R.prop('values'),
});

export default { actions, selectors };
