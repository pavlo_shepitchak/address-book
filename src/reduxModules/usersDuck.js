// @flow
import { handleActions } from 'redux-actions';
import * as R from 'ramda';
import { combineEpics, ofType } from 'redux-observable';
import { ajax } from 'rxjs/ajax';
import { catchError, map, concatMap } from 'rxjs/operators';
import { API_ENDPOINT, BATCH_SIZE } from '../config';
import { ReducerRegistry } from '../core';
import EpicRegistry from '../core/EpicRegistry';
import { createActionFactory, createGlobalizer } from '../utils';
import settingsDuck from './settingsDuck';

/**
 * This is redux module which responsible
 * for loading and storing users data provided by api
 *
 * Because this module contains logic shared between different screens,
 * we count him as 'global' , it shouldn't depend on any screen.
 * It's to avoid circular dependencies and support code splitting
 *
 * Because epic and reducer are mounting int the core of app on runtime,
 * this module will be lazy loaded only on first using
 * (When the first import statement will be executed).
 *
 * Module export object with actions and selector for interacting and selecting data.
 *
 * This part of redux store contains 2 properties
 * 1. byId  - store user in index (database-like style)
 * to make possible fast data access by id
 * 2. pages - store information about which userId belong to which number of page
 * it's an object, where the key is a number of page and the value is an array of userId's
 *
 * Fetching data available by 2 ways:
 * 1. by page, with limited number number of user fields,
 * it makes loading faster (needed for users list)
 * 2. by Id to load extended user information(used for Details screen)
 */
const moduleName = 'USERS';
const createAction = createActionFactory(moduleName);

const actions = {
  loadPage: createAction('LOAD_PAGE'),
  reset: createAction('RESET'),
  loadPageSuccess: createAction('LOAD_PAGE_SUCCESS'),
  loadPageError: createAction('LOAD_PAGE_ERROR'),
  loadById: createAction('LOAD_BY_ID'),
  loadByIdSuccess: createAction('LOAD_BY_ID_SUCCESS'),
  loadByIdError: createAction('LOAD_BY_ID_ERROR'),
};

const toIndex = R.compose(
  R.mergeAll,
  R.map(a => R.objOf(a.login.uuid, a))
);
const extractId = R.map(R.path(['login', 'uuid']));

const defaultState = {
  byId: {},
  pages: {},
};
const reducer = handleActions(
  {
    [actions.loadPageSuccess.toString()]: (state, { payload }) =>
      R.mergeDeepLeft(state, {
        byId: toIndex(payload.results),
        pages: { [payload.info.page]: extractId(payload.results) },
      }),
    [actions.loadByIdSuccess.toString()]: (state, { payload: { user, id } }) =>
      R.assocPath(['byId', id], user, state),
    [actions.reset.toString()]: R.always(defaultState),
  },
  defaultState
);
ReducerRegistry.register(moduleName, reducer);
//this method loads only field needed for home screen
const requestPage = (pageNumber, nationality) =>
  ajax.getJSON(
    `${API_ENDPOINT}?page=${pageNumber}&results=${BATCH_SIZE}&inc=login,picture,name,email,nat&seed=foobar${
      nationality ? `&nat=${nationality}` : ''
    }`
  );
//This this method IMITATE loading by id
// (but actually it loading always random user, because api doesn't support it)
const requestUser = (id, nationality) =>
  ajax
    .getJSON(
      `${API_ENDPOINT}?uuid=${id}?seed=foobar${
        nationality ? `&nat=${nationality}` : ''
      }`
    )
    .pipe(map(({ results }) => results[0]));
const loadPageEpic = (action$, state$) =>
  action$.pipe(
    ofType(actions.loadPage.toString()),
    concatMap(({ payload: page }) => {
      const { nationality } = settingsDuck.selectors.getAll(state$.value);
      return requestPage(page, nationality).pipe(
        map(actions.loadPageSuccess),
        catchError(error => [actions.loadPageError(error)])
      );
    })
  );
const loadByIdEpic = (action$, state$) =>
  action$.pipe(
    ofType(actions.loadById.toString()),
    concatMap(({ payload: id }) => {
      const { nationality } = settingsDuck.selectors.getAll(state$.value);
      return requestUser(id, nationality).pipe(
        map(user => actions.loadByIdSuccess({ user, id })),
        catchError(error => [actions.loadByIdError(error)])
      );
    })
  );
const epic = combineEpics(loadPageEpic, loadByIdEpic);
EpicRegistry.register(epic);

const selectors = createGlobalizer(moduleName)({
  getPage: (state, number) => R.pathOr(null, ['pages', number], state),
  getById: (state, id) => R.pathOr(null, ['byId', id], state),
});

export default { actions, epic, selectors };
