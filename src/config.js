export const API_ENDPOINT = 'https://randomuser.me/api';
export const MAX_CATALOG_SIZE = 1000;
export const BATCH_SIZE = 30;
export const ENABLE_PRELOADING = true;
