import React from 'react';
import { StoreProvider } from './core';
import Router from './screens';

const App = () => (
  <StoreProvider>
    <Router />
  </StoreProvider>
);
export default App;
