import Container from './Container';
import duck from './duck';

export default { ...duck, Component: Container };
