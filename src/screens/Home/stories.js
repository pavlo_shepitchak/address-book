import React from 'react';

import { addDecorator, storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { text, withKnobs } from '@storybook/addon-knobs';
import Presentation from './Presentation';
import UserListItem from '../Home/UserListItem';
import dummyData from '../../stories/dummyData';

addDecorator(withKnobs);
const onUserClick = action('listItemClick');
storiesOf('Home', module)
  .add('UserListItem', () => {
    const user = {
      avatar: text(
        'avatar',
        'https://randomuser.me/api/portraits/thumb/men/26.jpg'
      ),
      firstName: text('firstName', 'frankie'),
      lastName: text('lastName', 'boyd'),
      username: text('username', 'purplebird448'),
      email: text('email', 'frankie.boyd@example.com'),
    };
    return <UserListItem onClick={onUserClick} {...user} />;
  })
  .add('UserList', () => (
    <Presentation users={dummyData} onUserClick={onUserClick} />
  ));
