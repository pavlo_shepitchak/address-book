// @flow
// eslint-disable-next-line react-hooks/exhaustive-deps
import ListItem from '@material-ui/core/ListItem';
import React, { useCallback } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';

type Props = {|
  id: string,
  avatar: string,
  firstName: string,
  lastName: string,
  username: string,
  email: string,
  onClick: string => void,
|};
const UserListItem = ({
  id,
  avatar,
  firstName,
  lastName,
  email,
  username,
  onClick,
}: Props) => {
  // eslint-disable-next-line
  const clickHandler = useCallback(() => onClick(id), [id]);
  return (
    <Link
      to={{
        pathname: `details/${id}`,
        state: { modal: true },
      }}
    >
      <ListItemStyled onClick={clickHandler} component={'li'}>
        <ListItemAvatar>
          <Avatar alt="avatar" src={avatar} />
        </ListItemAvatar>
        <ListItemText
          primary={`${firstName} ${lastName} (${username})`}
          secondary={email}
        />
      </ListItemStyled>
    </Link>
  );
};
export default React.memo<Props>(UserListItem);

const ListItemStyled = styled(ListItem)`
  :hover {
    cursor: pointer;
  }
`;
