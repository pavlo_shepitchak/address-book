// @flow
import { makeStyles } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import InputBase from '@material-ui/core/InputBase';
import { fade } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import SettingsIcon from '@material-ui/icons/Settings';
import ListSubheader from '@material-ui/core/ListSubheader';
import * as R from 'ramda';

import React, { useCallback } from 'react';
import styled from 'styled-components';

type Props = {
  value: string,
  onChange: string => void,
  goToSettings: () => void,
};
const SearchField = ({ value, onChange, openSettings }: Props) => {
  const classes = useStyles();
  const changeHandler = useCallback(
    e => onChange(R.path(['target', 'value'], e)),
    [onChange]
  );
  return (
    <ListSubheaderStyled disableGutters>
      <div className={classes.search}>
        <div className={classes.searchIcon}>
          <SearchIcon />
        </div>
        <InputBase
          placeholder="Search…"
          classes={{
            root: classes.inputRoot,
            input: classes.inputInput,
          }}
          value={value}
          onChange={changeHandler}
        />
      </div>
      <IconButton
        edge="start"
        color="inherit"
        aria-label="Menu"
        onClick={openSettings}
      >
        <SettingsIcon />
      </IconButton>
    </ListSubheaderStyled>
  );
};
export default SearchField;
const useStyles = makeStyles(theme => ({
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.9),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 1),
    },
    boxShadow: '0px 1px 4px 1px rgba(0,0,0,0.4)',
    marginRight: theme.spacing(2),
    marginLeft: 0,
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
  },
}));

const ListSubheaderStyled = styled(ListSubheader)`
display: flex;
`;
