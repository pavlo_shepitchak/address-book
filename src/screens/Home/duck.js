import * as R from 'ramda';
import { handleActions } from 'redux-actions';
import { ofType } from 'redux-observable';
import * as Rx from 'rxjs';
import {
  first,
  filter,
  map,
  mergeMap,
  exhaustMap,
  takeUntil,
  ignoreElements,
  mergeMapTo,
} from 'rxjs/operators';
import { BATCH_SIZE, ENABLE_PRELOADING, MAX_CATALOG_SIZE } from '../../config';
import { EpicRegistry, ReducerRegistry } from '../../core';
import { createActionFactory, createGlobalizer } from '../../utils';
import { usersDuck } from '../../reduxModules';
import moduleName from './moduleName';

const createAction = createActionFactory(moduleName);
const actions = {
  init: createAction('INIT'), //fires when users list is mounted
  loadMore: createAction('LOAD_MORE'), // fires when user
  addItems: createAction('ADD_ITEMS'),
  setLoading: createAction('SET_LOADING'),
  userItemClicked: createAction('USER_ITEM_CLICKED'),
};

const defaultState = {
  data: [],
  currentPage: 0,
};
const reducer = handleActions(
  {
    [actions.init.toString()]: R.always(defaultState),
    [actions.addItems.toString()]: (state, { payload: { data, page } }) => ({
      data: R.concat(state.data, data),
      currentPage: page,
    }),
    [actions.setLoading.toString()]: (state, { payload }) =>
      R.assoc('isLoading', payload, state),
  },
  defaultState
);
const selectors = createGlobalizer(moduleName)({
  getData: R.prop('data'), //get array of userIDs
  getCurrentPage: R.prop('currentPage'),
  getIsLoading: R.prop('isLoading'),
});

/**
 *  on every action init or loadMore epic checks if next page is already available
  in usersDuck then just select it, if no load this data
 Every this time new page is fetching in advance
 */

const epic = (action$, state$) =>
  Rx.merge(
    action$.pipe(
      ofType(actions.init.toString()),
      mergeMapTo([usersDuck.actions.reset(), actions.loadMore()])
    ),
    action$.pipe(
      ofType(actions.loadMore.toString()),
      map(() => selectors.getData(state$.value).length),
      filter(length => length + BATCH_SIZE <= MAX_CATALOG_SIZE),
      exhaustMap(() => {
        const currentPage = selectors.getCurrentPage(state$.value);
        const nextPage = currentPage + 1;
        const pageData = usersDuck.selectors.getPage(state$.value, nextPage);
        const success$ = action$.pipe(
          ofType(usersDuck.actions.loadPageSuccess.toString()),
          map(() => usersDuck.selectors.getPage(state$.value, nextPage)),
          map(R.values)
        );
        const error$ = action$.pipe(
          ofType(usersDuck.actions.loadPageError.toString())
        );
        const load$ = Rx.concat(
          [usersDuck.actions.loadPage(nextPage), actions.setLoading(true)],
          Rx.race(success$, error$).pipe(
            first(),
            ignoreElements()
          ),
          [actions.setLoading(false)]
        );
        return Rx.merge(
          (pageData ? Rx.of(pageData) : success$).pipe(
            first(),
            mergeMap(data => {
              const result = [actions.addItems({ data, page: nextPage })];
              if (ENABLE_PRELOADING) {
                result.push(usersDuck.actions.loadPage(nextPage + 1));
              }
              return result;
            }),
            takeUntil(error$)
          ),
          !pageData ? load$ : Rx.EMPTY
        );
      })
    )
  );
ReducerRegistry.register(moduleName, reducer);
EpicRegistry.register(epic);
export default { selectors, actions };
