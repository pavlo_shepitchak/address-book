// @flow
import * as R from 'ramda';
import React, { useCallback, useEffect } from 'react';
import { connect } from 'react-redux';
import { usersDuck } from '../../reduxModules';
import duck from './duck';
import moduleName from './moduleName';
import Presentation, { type Props } from './Presentation';

const mapStateToProps = state => {
  //select array of ids, which should be shown in the list
  const idsToShow = duck.selectors.getData(state);
  //map each id to user data object
  const users = R.map(id => usersDuck.selectors.getById(state, id), idsToShow);
  const isLoading = duck.selectors.getIsLoading(state);
  return { users, isLoading };
};

const mapDispatchToProps = {
  init: duck.actions.init,
  loadMore: duck.actions.loadMore,
  onUserClick: duck.actions.userItemClicked,
};
const useOpenSettings = props =>
  useCallback(() => {
    console.log('home');
    return props.history.push('/settings');
  }, [props.history]);
const Container = (props: { ...Props, init: () => void }) => {
  useEffect(() => {
    props.init();
    // eslint-disable-next-line
  }, []);
  const openSettings = useOpenSettings(props);
  return <Presentation {...props} openSettings={openSettings} />;
};
Container.displayName = `Container(${moduleName})`;
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Container);
