// @flow
import List from '@material-ui/core/List';
import LinearProgress from '@material-ui/core/LinearProgress';
import Divider from '@material-ui/core/Divider';
import React, { useMemo, useState, useEffect } from 'react';
import * as R from 'ramda';
import type { User } from '../../types';
import SearchField from './SearchField';
import UserListItem from './UserListItem';

export type Props = {
  users: User[],
  onUserClick: string => void,
  loadMore: () => void,
  isLoading: boolean,
  openSettings: () => void,
};

const SCROLL_THRESHOLD = 50;
const useScrollEnd = callback =>
  useEffect(() => {
    const handler = () => {
      document.documentElement.offsetHeight -
        window.innerHeight -
        document.documentElement.scrollTop <
        SCROLL_THRESHOLD && callback();
    };
    document.addEventListener('scroll', handler);
    return () => document.removeEventListener('scroll', handler);
  }, []);

const Presentation = ({
  users,
  onUserClick,
  loadMore,
  isLoading,
  openSettings,
}: Props) => {
  const [searchValue, changeSearchValue] = useState('');
  const data = useFilter(searchValue, users);
  useScrollEnd(loadMore);
  const list = useRenderList(onUserClick, data);
  return (
    <List
      component="ul"
      subheader={
        <SearchField
          value={searchValue}
          onChange={changeSearchValue}
          openSettings={openSettings}
        />
      }
    >
      {list}
      {isLoading && <LinearProgress />}
    </List>
  );
};
export default Presentation;
const useRenderList = (onUserClick, data) =>
  useMemo(
    () =>
      data.map((user: User, index, array) => (
        <React.Fragment key={user.login.uuid}>
          <UserListItem
            id={user.login.uuid}
            avatar={user.picture.thumbnail}
            lastName={user.name.last}
            firstName={user.name.first}
            username={user.login.username}
            email={user.email}
            onClick={onUserClick}
          />
          {index !== array.length - 1 && <Divider />}
        </React.Fragment>
      )),
    [onUserClick, data]
  );
const useFilter = (query: string, data: User[]) =>
  useMemo(
    () =>
      R.filter(
        ({ name }) => `${name.first} ${name.last}`.includes(query),
        data
      ),
    [query, data]
  );
