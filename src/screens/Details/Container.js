// @flow
import * as R from 'ramda';
import React, { useCallback, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { usersDuck } from '../../reduxModules';
import duck from './duck';
import Presentation, { type Props } from './Presentation';

const mapStateToProps = (state, { match }) => {
  const isLoading = duck.selectors.getIsLoading(state);
  const user = usersDuck.selectors.getById(state, match.params.id);
  return {
    street: R.path(['location', 'street'], user),
    state: R.path(['location', 'state'], user),
    postcode: R.path(['location', 'postcode:'], user),
    city: R.path(['location', 'city'], user),
    phone: R.prop('phone', user),
    cell: R.prop('cell', user),
    isLoading,
  };
};

const mapDispatchToProps = {
  init: duck.actions.init,
};
const useBack = props =>
  useCallback(() => props.history.goBack(), [props.history.goBack]);
const useInit = props =>
  useEffect(() => {
    props.init(props.match.params.id);
    // eslint-disable-next-line
  }, []);
const Container = (props: { ...Props, init: string => void }) => {
  useInit(props);
  const onClose = useBack(props);
  return <Presentation {...props} onClose={onClose} />;
};
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Container)
);
