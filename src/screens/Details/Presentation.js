// @flow
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import React from 'react';
import styled from 'styled-components';
import Modal from '@material-ui/core/Modal';
import CircularProgress from '@material-ui/core/CircularProgress';

type Props = {
  street: string,
  state: string,
  postcode: string,
  phone: string,
  cell: string,
  city: string,
  onClose: () => void,
  isLoading: boolean,
};
const Presentation = ({
  street,
  state,
  postcode,
  phone,
  city,
  cell,
  onClose,
  isLoading,
}: Props) => {
  return (
    <Modal open onClose={onClose} style={{ width: '100%' }}>
      <ListStyled component="ul">
        {isLoading && <CircularProgressStyled />}
        <PaperStyled hide={isLoading}>
          {[
            { value: street, name: 'Street' },
            { value: city, name: 'City' },
            { value: state, name: 'State' },
            { value: postcode, name: 'Postcode' },
            { value: phone, name: 'Phone' },
            { value: cell, name: 'Cell' },
          ].map(renderRow)}
        </PaperStyled>
      </ListStyled>
    </Modal>
  );
};
const renderRow = ({ name, value }) => (
  <ListItem key={name} component={'li'}>
    <ListItemTextStyled primary={name} />
    <ListItemTextStyled primary={value} />
  </ListItem>
);
const ListItemTextStyled = styled(ListItemText)`
  width: 50%;
`;
const ListStyled = styled(List)`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`;
const PaperStyled = styled(Paper)`
  visibility: ${props => (props.hide ? 'hidden' : 'visible')};
  width: 50%;
`;
const CircularProgressStyled = styled(CircularProgress)`
  width: 100px !important;
  height: 100px !important;
  align-self: center;
  justify-self: center;
`;
export default React.memo<Props>(Presentation);
