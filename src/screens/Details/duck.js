import * as R from 'ramda';
import { handleActions } from 'redux-actions';
import { ofType } from 'redux-observable';
import * as Rx from 'rxjs';
import { first, mergeMap, ignoreElements } from 'rxjs/operators';
import { EpicRegistry, ReducerRegistry } from '../../core';
import { createActionFactory, createGlobalizer } from '../../utils';
import { usersDuck } from '../../reduxModules';
import moduleName from './moduleName';

const createAction = createActionFactory(moduleName);
const actions = {
  init: createAction('INIT'),
  setLoading: createAction('SET_LOADING'),
};

const defaultState = { isLoading: false };
const reducer = handleActions(
  {
    [actions.setLoading.toString()]: (state, { payload }) =>
      R.assoc('isLoading', payload, state),
  },
  defaultState
);
const selectors = createGlobalizer(moduleName)({
  getIsLoading: R.prop('isLoading'),
});

const epic = action$ =>
  action$.pipe(
    ofType(actions.init.toString()),
    mergeMap(({ payload: id }) =>
      Rx.concat(
        [actions.setLoading(true), usersDuck.actions.loadById(id)],
        Rx.race(
          action$.pipe(ofType(usersDuck.actions.loadByIdSuccess.toString())),
          action$.pipe(ofType(usersDuck.actions.loadByIdError.toString()))
        ).pipe(
          first(),
          ignoreElements()
        ),
        [actions.setLoading(false)]
      )
    )
  );
ReducerRegistry.register(moduleName, reducer);
EpicRegistry.register(epic);
export default { selectors, actions };
