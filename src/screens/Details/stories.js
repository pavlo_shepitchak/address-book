import React from 'react';

import { addDecorator, storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { text, withKnobs } from '@storybook/addon-knobs';
import Presentation from './Presentation';

addDecorator(withKnobs);
const onUserClick = action('listItemClick');
storiesOf('Details', module).add('Details', () => {
  const address = {
    street: text(
      'street',
      '4772 calle de argumosa'
    ),
    state: text('state', 'cataluña'),
    city: text('city', 'zaragoza'),
    postcode: text('postcode', 36165),
    phone: text('phone', '932-569-326'),
    cell: text('cell', '676-821-706'),
  };
  return <Presentation onClick={onUserClick} {...address} />;
});
