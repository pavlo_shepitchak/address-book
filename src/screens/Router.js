import * as R from 'ramda';
import React from 'react';
import { Switch } from 'react-router';
import { BrowserRouter, Route } from 'react-router-dom';

const extractC = R.compose(
  R.objOf('default'),
  R.path(['default', 'Component'])
);
const AsyncDetails = React.lazy(() => import('./Details').then(extractC));
const AsyncHome = React.lazy(() => import('./Home').then(extractC));
const AsyncSettings = React.lazy(() => import('./Settings').then(extractC));

class ModalSwitch extends React.Component {
  componentWillUpdate(nextProps) {
    let { location } = this.props;
    if (
      nextProps.history.action !== 'POP' &&
      (!location.state || !location.state.modal)
    ) {
      this.previousLocation = this.props.location;
    }
  }
  render() {
    const { location } = this.props;

    const isModal = !!(
      location.state &&
      location.state.modal &&
      this.previousLocation !== location
    );

    return (
      <div>
        <Switch location={isModal ? this.previousLocation : location}>
          <Route path="/settings" exact component={AsyncSettings} />
          <Route path="/*" exact component={AsyncHome} />
        </Switch>
        {isModal && <Route path="/details/:id" component={AsyncDetails} />}
      </div>
    );
  }
}

const Router = () => (
  <React.Suspense fallback={<div>Loading...</div>}>
    <BrowserRouter>
      <Route component={ModalSwitch} />
    </BrowserRouter>
  </React.Suspense>
);

export default React.memo(Router);
