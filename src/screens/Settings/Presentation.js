// @flow
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import SearchIcon from '@material-ui/icons/ArrowBack';
import React, { useCallback } from 'react';
import styled from 'styled-components';
import moduleName from './moduleName';

type Props = {
  goBack: () => void,
  goHome: () => void,
  values: { [string]: string },
  set: ({ [string]: string }) => void,
};
const Presentation = ({ goBack, goHome, values, set }: Props) => {
  const onChange = useCallback(e => set({ [e.target.name]: e.target.value }));
  return (
    <RootView>
      <AppBar position="static">
        <ToolbarStyled>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="Menu"
            onClick={goBack}
          >
            <SearchIcon />
          </IconButton>
          <Typography variant="h6">Settings</Typography>
          <Button color="inherit" onClick={goHome}>
            Home
          </Button>
        </ToolbarStyled>
      </AppBar>
      <FormControl>
        <InputLabel>Nationality</InputLabel>
        <Select
          value={values['nationality']}
          onChange={onChange}
          input={<Input name="nationality" />}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {['CH', 'ES', 'FR', 'GB'].map(item => (
            <MenuItem value={item} key={item}>
              {item}
            </MenuItem>
          ))}
        </Select>
        <FormHelperText>Some important helper text</FormHelperText>
      </FormControl>
    </RootView>
  );
};

const ToolbarStyled = styled(Toolbar)`
  display: flex;
  justify-content: space-between;
`;
export default Presentation;
Presentation.displayName = `Presentation(${moduleName})`;
const RootView = styled.div``;
