// @flow
import React, { useCallback } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { settingsDuck } from '../../reduxModules';
import Presentation, { type Props } from './Presentation';

const mapStateToProps = state => ({
  values: settingsDuck.selectors.getAll(state),
});

const mapDispatchToProps = {
  set: settingsDuck.actions.set,
};
const useBack = props =>
  useCallback(() => props.history.goBack(), [props.history]);
const useGoHome = props =>
  useCallback(() => {
    console.log('home');
    return props.history.push('/');
  }, [props.history]);
const Container = (props: Props) => {
  const onClose = useBack(props);
  const goHome = useGoHome(props);
  return <Presentation {...props} goBack={onClose} goHome={goHome} />;
};
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Container)
);
