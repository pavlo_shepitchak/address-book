import * as R from 'ramda';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { combineEpics, createEpicMiddleware } from 'redux-observable';
import { BehaviorSubject } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import ReducerRegistry from './ReducerRegistry';
import EpicRegistry from './EpicRegistry';
/**
 * Function which includes logic related to configuring redux store,
 * dynamic epics and reducers logic
 */
export default function() {
  const epic$ = new BehaviorSubject(combineEpics());
  const rootEpic = (action$, state$, deps) =>
    epic$.pipe(mergeMap(epic => epic(action$, state$, deps)));
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const reducerMap = ReducerRegistry.getReducers();
  const rootReducer = R.isEmpty(reducerMap)
    ? R.identity
    : combineReducers(reducerMap);
  const epicMiddleware = createEpicMiddleware();
  const store = createStore(
    rootReducer,
    {},
    composeEnhancers(applyMiddleware(epicMiddleware))
  );
  epicMiddleware.run(rootEpic);
  EpicRegistry.setChangeListener(epic => epic$.next(epic));
  ReducerRegistry.setChangeListener(reducers =>
    store.replaceReducer(combineReducers(reducers))
  );
  return store;
}
