import React, { useMemo } from 'react';
import { Provider } from 'react-redux';
import configureStore from './configureStore';

/**
 *
 * React component which is used to wrap the app with redux StoreProvider
 */
const StoreProvider = ({ children }) => {
  const store = useMemo(configureStore, []);
  return <Provider store={store}>{children}</Provider>;
};

export default StoreProvider;
