// @flow
/**
 * Class which is used for loading epics on the fly
 * It's needed for code splitting and keeping application modular
 */
import * as Rx from 'rxjs';

type Epic = (
  Rx.Observable<*>,
  Rx.Observable<*>,
  { api: { request: Function } }
) => Rx.Observable<*>;
let onChangeHandler: ?(Epic) => void = null;
let _epics = [];
const setChangeListener = (handler: typeof onChangeHandler) => {
  if (!handler) return;
  onChangeHandler = handler;
  _epics.map(handler);
  _epics = [];
};

const register = (epic: Epic) =>
  onChangeHandler ? onChangeHandler(epic) : _epics.push(epic);
export default { setChangeListener, register };
