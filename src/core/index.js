/**
 * index file helps have an optimized imports
 */
export { default as EpicRegistry } from './EpicRegistry';
export { default as ReducerRegistry } from './ReducerRegistry';
export { default as StoreProvider } from './StoreProvider';
