export default [
  {
    gender: 'female',
    name: {
      title: 'ms',
      first: 'mathilde',
      last: 'madsen',
    },
    location: {
      street: '8997 enebærvej',
      city: 'viby sj.',
      state: 'nordjylland',
      postcode: 26674,
      coordinates: {
        latitude: '-36.5337',
        longitude: '179.4383',
      },
      timezone: {
        offset: '-9:00',
        description: 'Alaska',
      },
    },
    email: 'mathilde.madsen@example.com',
    login: {
      uuid: 'a7d1e2f9-2bce-4a81-8179-91e024fdd925',
      username: 'organicmeercat173',
      password: 'jeanette',
      salt: 'RU3QGhVe',
      md5: '7d5ed107a5aa254a1f821201aa08280d',
      sha1: '9d772f5887cab9b94a57dc5c04b9c9a41e012cb1',
      sha256:
        '25bd37997ed6617bf29938167719eb9bfbe12b298146c732b1d798e9035d2fc9',
    },
    dob: {
      date: '1991-07-14T04:38:28Z',
      age: 27,
    },
    registered: {
      date: '2002-09-05T12:29:32Z',
      age: 16,
    },
    phone: '39886138',
    cell: '65698062',
    id: {
      name: 'CPR',
      value: '408608-1509',
    },
    picture: {
      large: 'https://randomuser.me/api/portraits/women/17.jpg',
      medium: 'https://randomuser.me/api/portraits/med/women/17.jpg',
      thumbnail: 'https://randomuser.me/api/portraits/thumb/women/17.jpg',
    },
    nat: 'DK',
  },
  {
    gender: 'male',
    name: {
      title: 'mr',
      first: 'antonio',
      last: 'garcia',
    },
    location: {
      street: '4772 calle de argumosa',
      city: 'zaragoza',
      state: 'cataluña',
      postcode: 36165,
      coordinates: {
        latitude: '60.1632',
        longitude: '60.3739',
      },
      timezone: {
        offset: '+3:30',
        description: 'Tehran',
      },
    },
    email: 'antonio.garcia@example.com',
    login: {
      uuid: '89ee0a47-1d90-4d18-9f53-25975f6404fb',
      username: 'greenmouse175',
      password: 'sabrina',
      salt: 'bq7UuVF8',
      md5: '23268fa49a26fd9099ac1a8c005fb551',
      sha1: 'd1defd89cb8690a156f85e2a8ce13f18b85506b7',
      sha256:
        'f25a9ef8b0c48ff1843ff632d1b63528929bc418e52e75f7ce0215e2b0e23e3a',
    },
    dob: {
      date: '1970-07-31T09:07:54Z',
      age: 48,
    },
    registered: {
      date: '2005-01-31T06:59:25Z',
      age: 14,
    },
    phone: '932-569-326',
    cell: '676-821-706',
    id: {
      name: 'DNI',
      value: '66159464-Z',
    },
    picture: {
      large: 'https://randomuser.me/api/portraits/men/6.jpg',
      medium: 'https://randomuser.me/api/portraits/med/men/6.jpg',
      thumbnail: 'https://randomuser.me/api/portraits/thumb/men/6.jpg',
    },
    nat: 'ES',
  },
  {
    gender: 'male',
    name: {
      title: 'mr',
      first: 'andreas',
      last: 'jørgensen',
    },
    location: {
      street: '5364 egehegnet',
      city: 'bælum',
      state: 'syddanmark',
      postcode: 86155,
      coordinates: {
        latitude: '-30.3298',
        longitude: '94.6373',
      },
      timezone: {
        offset: '-4:00',
        description: 'Atlantic Time (Canada), Caracas, La Paz',
      },
    },
    email: 'andreas.jørgensen@example.com',
    login: {
      uuid: '8fc5a172-1539-4af5-8ac9-d31b0ab929f7',
      username: 'yellowdog541',
      password: 'deng',
      salt: 'ea6P19y1',
      md5: '8372f4526f01feae3e6c5ddfec5caabe',
      sha1: '76360b54c174d09acbf4a85fd365858b9f5a6d43',
      sha256:
        '81aed9f528bdd4dbeb799deef9c12b0455f2d10e2d2bdc8b29657fd331fdb8fc',
    },
    dob: {
      date: '1962-02-01T10:34:11Z',
      age: 57,
    },
    registered: {
      date: '2009-10-05T19:21:08Z',
      age: 9,
    },
    phone: '85359617',
    cell: '98972217',
    id: {
      name: 'CPR',
      value: '244646-7698',
    },
    picture: {
      large: 'https://randomuser.me/api/portraits/men/2.jpg',
      medium: 'https://randomuser.me/api/portraits/med/men/2.jpg',
      thumbnail: 'https://randomuser.me/api/portraits/thumb/men/2.jpg',
    },
    nat: 'DK',
  },
  {
    gender: 'female',
    name: {
      title: 'mrs',
      first: 'liva',
      last: 'madsen',
    },
    location: {
      street: '1563 hasselvej',
      city: 'københavn v',
      state: 'hovedstaden',
      postcode: 59616,
      coordinates: {
        latitude: '32.3181',
        longitude: '-29.9221',
      },
      timezone: {
        offset: '+9:00',
        description: 'Tokyo, Seoul, Osaka, Sapporo, Yakutsk',
      },
    },
    email: 'liva.madsen@example.com',
    login: {
      uuid: 'f4215482-2ed1-40ea-9f6d-4f27570d858f',
      username: 'blackleopard656',
      password: 'puppet',
      salt: 'AMsQizDq',
      md5: '6980b6865bf04bfd804ac8e001c72c07',
      sha1: 'c0b716abdc366cb956081985f66f356dd5d4b41b',
      sha256:
        '5eeb3ef84cbde1fd2d759f57596a3818f6d558b4ebcc33f1b34859e6f2054184',
    },
    dob: {
      date: '1978-06-21T02:42:56Z',
      age: 40,
    },
    registered: {
      date: '2006-12-14T11:18:29Z',
      age: 12,
    },
    phone: '29322223',
    cell: '30236325',
    id: {
      name: 'CPR',
      value: '539797-5099',
    },
    picture: {
      large: 'https://randomuser.me/api/portraits/women/58.jpg',
      medium: 'https://randomuser.me/api/portraits/med/women/58.jpg',
      thumbnail: 'https://randomuser.me/api/portraits/thumb/women/58.jpg',
    },
    nat: 'DK',
  },
  {
    gender: 'male',
    name: {
      title: 'mr',
      first: 'bob',
      last: 'richardson',
    },
    location: {
      street: '7691 paddock way',
      city: 'wagga wagga',
      state: 'tasmania',
      postcode: 4976,
      coordinates: {
        latitude: '30.9095',
        longitude: '-142.4574',
      },
      timezone: {
        offset: '-2:00',
        description: 'Mid-Atlantic',
      },
    },
    email: 'bob.richardson@example.com',
    login: {
      uuid: 'bedfc341-c467-4b6a-8afd-4f879e247586',
      username: 'tinycat128',
      password: 'chevy1',
      salt: 'ZHyZBka1',
      md5: 'f518f8f8817db24b3c77cd82b8ab61f3',
      sha1: 'bf652f2453646b3bab065d284949e9345a1d2dbe',
      sha256:
        'f853dfca28d5ef6220a73e7a041f69957e142cab604c6918402a295e81af33cb',
    },
    dob: {
      date: '1996-04-12T03:00:49Z',
      age: 23,
    },
    registered: {
      date: '2016-12-29T12:52:14Z',
      age: 2,
    },
    phone: '09-7081-1679',
    cell: '0485-535-227',
    id: {
      name: 'TFN',
      value: '715684649',
    },
    picture: {
      large: 'https://randomuser.me/api/portraits/men/26.jpg',
      medium: 'https://randomuser.me/api/portraits/med/men/26.jpg',
      thumbnail: 'https://randomuser.me/api/portraits/thumb/men/26.jpg',
    },
    nat: 'AU',
  },
  {
    gender: 'male',
    name: {
      title: 'mr',
      first: 'frankie',
      last: 'boyd',
    },
    location: {
      street: '1230 oaks cross',
      city: 'derby',
      state: 'wiltshire',
      postcode: 'Y27 6BN',
      coordinates: {
        latitude: '22.2633',
        longitude: '-71.5428',
      },
      timezone: {
        offset: '+4:30',
        description: 'Kabul',
      },
    },
    email: 'frankie.boyd@example.com',
    login: {
      uuid: 'a2674b28-9a49-43e6-b333-45135a53995e',
      username: 'purplebird448',
      password: 'disco',
      salt: '8eXWExSY',
      md5: 'a7af0057cb0416a982fe363a90265ce5',
      sha1: '9ba3f8a0ff6a3820b00ee5efbff34d44a52f2653',
      sha256:
        '7f9386fa98496dc62bb5a7706d1d77432e65b5def608db834f06f46fcdcffeeb',
    },
    dob: {
      date: '1954-05-24T00:44:45Z',
      age: 64,
    },
    registered: {
      date: '2015-01-09T10:56:30Z',
      age: 4,
    },
    phone: '017684 42282',
    cell: '0758-461-265',
    id: {
      name: 'NINO',
      value: 'JN 17 69 11 Q',
    },
    picture: {
      large: 'https://randomuser.me/api/portraits/men/40.jpg',
      medium: 'https://randomuser.me/api/portraits/med/men/40.jpg',
      thumbnail: 'https://randomuser.me/api/portraits/thumb/men/40.jpg',
    },
    nat: 'GB',
  },
  {
    gender: 'female',
    name: {
      title: 'miss',
      first: 'ece',
      last: 'tunaboylu',
    },
    location: {
      street: '2397 filistin cd',
      city: 'konya',
      state: 'malatya',
      postcode: 86732,
      coordinates: {
        latitude: '42.5165',
        longitude: '148.2287',
      },
      timezone: {
        offset: '+11:00',
        description: 'Magadan, Solomon Islands, New Caledonia',
      },
    },
    email: 'ece.tunaboylu@example.com',
    login: {
      uuid: 'c80ee051-c659-4eb8-986d-720d3e20fe4e',
      username: 'brownbird984',
      password: 'michelle',
      salt: 'XF2UVX30',
      md5: '78f1570c149578bb5c8c389cf37d8666',
      sha1: '9b14149f4227c763e217bb6efca0b4f74fb11947',
      sha256:
        '9d4ce59b5e6aa90325928c35c0ebb4f03570abc797d090284e249419fa6f9c97',
    },
    dob: {
      date: '1983-02-20T16:09:36Z',
      age: 36,
    },
    registered: {
      date: '2006-11-06T09:19:39Z',
      age: 12,
    },
    phone: '(210)-246-5133',
    cell: '(243)-153-5098',
    id: {
      name: '',
      value: null,
    },
    picture: {
      large: 'https://randomuser.me/api/portraits/women/87.jpg',
      medium: 'https://randomuser.me/api/portraits/med/women/87.jpg',
      thumbnail: 'https://randomuser.me/api/portraits/thumb/women/87.jpg',
    },
    nat: 'TR',
  },
  {
    gender: 'male',
    name: {
      title: 'mr',
      first: 'célsio',
      last: 'lopes',
    },
    location: {
      street: '6739 rua são pedro ',
      city: 'caucaia',
      state: 'pará',
      postcode: 24199,
      coordinates: {
        latitude: '38.5469',
        longitude: '-153.4012',
      },
      timezone: {
        offset: '+7:00',
        description: 'Bangkok, Hanoi, Jakarta',
      },
    },
    email: 'célsio.lopes@example.com',
    login: {
      uuid: 'd239ced9-8593-43bc-a8ac-7a7f7c8451a1',
      username: 'smallostrich337',
      password: 'boytoy',
      salt: 'hd0cNtDv',
      md5: '57fd30ce31f9ae27cf90a3fa10d8420d',
      sha1: 'a639e93d6c88a21d63fab21f4486b8632dbe1b3c',
      sha256:
        '01a36502a30bbdeed217774a69f0566b23d0cc81d2148debf5fe8ada42b416a3',
    },
    dob: {
      date: '1980-08-18T00:52:22Z',
      age: 38,
    },
    registered: {
      date: '2011-01-20T00:03:58Z',
      age: 8,
    },
    phone: '(50) 7700-9653',
    cell: '(93) 7165-3875',
    id: {
      name: '',
      value: null,
    },
    picture: {
      large: 'https://randomuser.me/api/portraits/men/86.jpg',
      medium: 'https://randomuser.me/api/portraits/med/men/86.jpg',
      thumbnail: 'https://randomuser.me/api/portraits/thumb/men/86.jpg',
    },
    nat: 'BR',
  },
  {
    gender: 'male',
    name: {
      title: 'mr',
      first: 'gerco',
      last: 'veldkamp',
    },
    location: {
      street: '9753 hoefijzerstraat',
      city: 'barneveld',
      state: 'noord-brabant',
      postcode: 67096,
      coordinates: {
        latitude: '9.9058',
        longitude: '-83.9600',
      },
      timezone: {
        offset: '-3:30',
        description: 'Newfoundland',
      },
    },
    email: 'gerco.veldkamp@example.com',
    login: {
      uuid: 'f0f1cc76-bd49-4162-8623-a9058fdca43a',
      username: 'greenfrog659',
      password: 'moneys',
      salt: 'CiBywUD8',
      md5: '1560a9af0b5c4e2ea2c5e6f10736cdf9',
      sha1: '930ea90bfb4c3b1bfe0671ae8d50e4c3434be323',
      sha256:
        '94c76b497d8a73b6acec47e77e340e86909da3939a13c1062a10c7670a5318fc',
    },
    dob: {
      date: '1985-01-11T05:58:16Z',
      age: 34,
    },
    registered: {
      date: '2010-03-25T06:40:32Z',
      age: 9,
    },
    phone: '(527)-291-8279',
    cell: '(788)-682-0499',
    id: {
      name: 'BSN',
      value: '75148495',
    },
    picture: {
      large: 'https://randomuser.me/api/portraits/men/82.jpg',
      medium: 'https://randomuser.me/api/portraits/med/men/82.jpg',
      thumbnail: 'https://randomuser.me/api/portraits/thumb/men/82.jpg',
    },
    nat: 'NL',
  },
  {
    gender: 'female',
    name: {
      title: 'mrs',
      first: 'ianthe',
      last: 'pap',
    },
    location: {
      street: '2629 willem van noortplein',
      city: 'hilversum',
      state: 'noord-brabant',
      postcode: 71843,
      coordinates: {
        latitude: '11.5397',
        longitude: '151.7431',
      },
      timezone: {
        offset: '-3:00',
        description: 'Brazil, Buenos Aires, Georgetown',
      },
    },
    email: 'ianthe.pap@example.com',
    login: {
      uuid: 'fb251570-4892-42cb-8d54-6dcab504488f',
      username: 'lazylion674',
      password: 'lilly',
      salt: 'teBDD0sm',
      md5: '5a8ded820f3164b09ae04bdec35b8013',
      sha1: '298e8934308325d4f658b1dfb81d3a6fa8da2be2',
      sha256:
        '79975b3fa17cfa0de21d2f9b1963580178a1e1bed01d3f20e28231f77d25a41d',
    },
    dob: {
      date: '1994-12-19T08:30:41Z',
      age: 24,
    },
    registered: {
      date: '2004-12-11T23:37:56Z',
      age: 14,
    },
    phone: '(965)-416-5534',
    cell: '(197)-670-7012',
    id: {
      name: 'BSN',
      value: '96027699',
    },
    picture: {
      large: 'https://randomuser.me/api/portraits/women/80.jpg',
      medium: 'https://randomuser.me/api/portraits/med/women/80.jpg',
      thumbnail: 'https://randomuser.me/api/portraits/thumb/women/80.jpg',
    },
    nat: 'NL',
  },
];
