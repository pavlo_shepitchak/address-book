// @flow
export type User = {
  picture: { thumbnail: string },
  name: {
    first: string,
    last: string,
    username: string,
  },
  email: string,
  location: {
    street: string,
    city: string,
    state: string,
    postcode: string,
  },
  login: { username: string, uuid: string },
  phone: string,
  cell: string,
};
