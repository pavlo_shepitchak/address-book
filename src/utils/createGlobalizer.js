import * as R from 'ramda';
import { createSelector } from 'reselect';

/**
 *
 * Creating function which converting map of local selector
 * to map of global selector with specified mount point
 *
 * @param mountPoint
 * @returns {Function}
 */
export default mountPoint =>
  R.map(sourceSelector =>
    createSelector(
      R.prop(mountPoint),
      (_, param) => param,
      sourceSelector
    )
  );
