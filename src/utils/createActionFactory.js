// @flow
import { createAction } from 'redux-actions';
/**
 *
 * Creating a prefixed version of ActionCreator
 *
 * @param {String} prefix
 * @returns {function(string): ActionCreator}
 */
export default <T>(prefix: string) => (action: string) =>
  createAction<string, T>(`${prefix}/${action}`);
